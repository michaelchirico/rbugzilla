# Rbugzilla

Some functions to get information from https://bugs.r-project.org into
R using the [Bugzilla rest
API](https://bugzilla.readthedocs.io/en/latest/api/).

Available for now:

| Function       | Purpose                                         |
|----------------|-------------------------------------------------|
|`bz_get_bugs`   | get list of one or more bugs specified by number|
|`bz_open_bugs`  | get list of all open bugs[^1]                   |

  
- The argument to `bz_get_bugs` is an integer vector of bug numbers.

- The results are lists of bug objects.

[^1]: There seems to be one problematic record that needs to be
omitted.
